import 'package:dio/dio.dart';
import 'package:shopifly/models/http_exception.dart';
import 'package:shopifly/resources/user.dart';

const HOST = "http://89.47.166.9:60080";
const LOGIN_PATH = "login";
const CARTS_VENDOR = 'carts_vendor';
const SUMMARY = 'summary';
const ORDER_STATUS_CUST_CONFIRMED = 'status=1';
const IN_PROGRESS_ORDERS = 'status=2';
const SENT_ORDERS = 'status=3';
const DELIVERED = 'status=4';
const REFUSED_ORDERS = 'status=5';

const SET_REFUSED = 'refuse';
const SET_CONFIRMED = 'confirm';
const SET_SENT_ORDERS = 'delivering';
const SET_DELIVERED = 'delivered';

class Api {
  static const int to_be_confirmed = 1;
  static const int confirmed = 2;
  static const int sent_orders = 3;
  static const int delivered = 4;
  static const int refused = 5;

  static Future<Response> login() async {
    Map<String, String> headers = {
      "Content-type": "application/json",
    };
    Response response = await DioWrapper.instance.getDio().post(
          "$HOST/$LOGIN_PATH",
          data: {"token": User().firebaseToken},
          options: Options(headers: headers),
        );
    return response;
  }

  static Future<Response> getCustOrders(int status) async {
    String url = "$HOST/$CARTS_VENDOR/$SUMMARY";
    switch (status) {
      case to_be_confirmed:
        {
          url = url + "?$ORDER_STATUS_CUST_CONFIRMED";
        }
        break;
      case refused:
        {
          url = url + "?$REFUSED_ORDERS";
        }
        break;
      case confirmed:
        {
          url = url + "?$IN_PROGRESS_ORDERS";
        }
        break;
      case sent_orders:
        {
          url = url + "?$SENT_ORDERS";
        }
        break;
      case delivered:
        {
          url = url + "?$delivered";
        }
        break;
    }

    print(url);

    Response response = await DioWrapper.instance.getDioWithJWT().get(url);
    if (response.statusCode >= 400) throw HttpException(response.statusMessage);

    return response;
  }

  static Future<void> setOrderStatus(int status, int idOrder) async {
    print('id ordineeeeeeeeeeeeeee $idOrder');
    String url = "$HOST/$CARTS_VENDOR/";
    switch (status) {
      case refused:
        {
          url = url + "$SET_REFUSED/";
        }
        break;
      case confirmed:
        {
          url = url + "$SET_CONFIRMED/";
        }
        break;
      case sent_orders:
        {
          url = url + "$SET_SENT_ORDERS/";
        }
        break;
      case delivered:
        {
          url = url + "$SET_DELIVERED/";
        }
        break;
    }

    url = url + '$idOrder';
    print(url);
    Response response = await DioWrapper.instance.getDioWithJWT().post(url);
    if (response.statusCode >= 400) throw HttpException(response.statusMessage);
  }
}

class DioWrapper {
  DioWrapper._privateConstructor();

  static final DioWrapper instance = DioWrapper._privateConstructor();
  final Dio _dio = Dio();

  Dio getDio() {
    return _dio;
  }

  Dio getDioWithJWT() {
    final Dio _dioWithJWT = Dio();

    if (User().jwt != null && User().jwt.isNotEmpty) {
      _dioWithJWT.options.headers["Authorization"] = "Bearer ${User().jwt}";
    }

    print(User().jwt);
    print('stampaaaaaaaaaaa ${_dioWithJWT.options.headers['Authorization']}');
    return _dioWithJWT;
  }
}
