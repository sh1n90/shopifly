import 'dart:async';

import 'package:geolocator/geolocator.dart';

class User {
  static final User _singleton = new User._internal();

  factory User() {
    return _singleton;
  }

  User._internal();

  String jwt;
  Position position;
  String userId;
  String firebaseToken;
  DateTime expiryDate;
  Timer authTimer;
}
