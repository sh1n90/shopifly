import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopifly/providers/auth.dart';
import 'package:shopifly/providers/orders.dart';
import 'package:shopifly/screens/authscreen.dart';
import 'package:shopifly/screens/mainscreen.dart';
import 'package:shopifly/screens/ordersscreen.dart';
import 'package:shopifly/screens/splashscreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.green),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    print('build main');
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => Auth()),
          ChangeNotifierProvider(create: (_) => Orders()),
        ],
        child: Consumer<Auth>(
          builder: (ctx, auth, _) => MaterialApp(
              title: 'xHop',
              theme: ThemeData(
                primarySwatch: Colors.blue,
                accentColor: Colors.yellow,
                fontFamily: 'song',
              ),
              home: auth.isAuth
                  ? MainScreen()
                  : FutureBuilder(
                      future: auth.tryAutoLogin(),
                      builder: (ctx, authResultSnapshot) =>
                          authResultSnapshot.connectionState ==
                                  ConnectionState.waiting
                              ? SplashScreen()
                              : AuthScreen(),
                    ),
              routes: {
                OrdersScreen.routeName: (ctx) => OrdersScreen(),
                // HomeScreen.routeName: (ctx) => HomeScreen()
                //  ProductDetailScreen.routeName: (ctx) =>
                //      ProductDetailScreen(),
              }),
        ));
  }
}
