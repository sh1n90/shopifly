import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopifly/api/api.dart';
import 'package:shopifly/providers/orders.dart';

class Order extends StatelessWidget {
  final idOrder;
  final totalAmount;
  final idStatusDelivery;
  final uniqueName;

  Order({
    @required this.idOrder,
    @required this.totalAmount,
    @required this.idStatusDelivery,
    @required this.uniqueName,
  });

  Widget build(BuildContext context) {
    final orderData = Provider.of<Orders>(context, listen: false);

    return Dismissible(
      key: ValueKey(idOrder),
      background: Container(
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction) {
        print('direzioneeeeeeeeeee ${direction.index}');
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text(
              'Do you want to remove the item from the cart?',
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(ctx).pop(false);
                },
              ),
              FlatButton(
                child: Text('Yes'),
                onPressed: () {
                  Navigator.of(ctx).pop(true);
                },
              ),
            ],
          ),
        );
      },
      onDismissed: (direction) {
        orderData.setRefusedOrders(idOrder);
      },
      child: Card(
        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListTile(
            leading: CircleAvatar(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: FittedBox(
                  child: Text('\$$totalAmount'),
                ),
              ),
            ),
            title: Text(' aaa $uniqueName'),
            trailing: FlatButton(
              color: Colors.blue,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.white)),
              child: Icon(
                Icons.check,
                color: Colors.white,
                size: 40,
              ),
              onPressed: () {
                switch (idStatusDelivery) {
                  case Api.to_be_confirmed:
                    {
                      print('case Api.to_be_confirmed');
                      orderData.setConfirmOrders(idOrder);
                      break;
                    }
                  case Api.confirmed:
                    {
                      orderData.setSentOrders(idOrder);
                      break;
                    }
                  case Api.sent_orders:
                    {
                      orderData.setDelivered(idOrder);
                      break;
                    }
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
