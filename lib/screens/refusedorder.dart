import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopifly/providers/orders.dart';

class RefusedOrders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final orderData = Provider.of<Orders>(context, listen: false);
    return (orderData.refusedOrders.isEmpty)
        ? FutureBuilder(
            future: orderData.getRefusedOrders(),
            builder: (ctx, dataSnapshot) {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                if (dataSnapshot.error != null) {
                  // ...
                  // Do error handling stuff
                  return Center(
                    child: Text('An error occurred!'),
                  );
                } else {
                  return Column(children: [
                    Row(
                      children: [
                        Title(
                          title: "Ordini Da confermare",
                          color: Colors.blue,
                        )
                      ],
                    ),
                    ListView.builder(
                        itemCount: orderData.refusedOrders.length,
                        itemBuilder: (ctx, i) =>
                            orderData.refusedOrders.elementAt(i))
                  ]);
                  //    Text('ciao');
                }
              }
            })
        : ListView.builder(
            itemCount: orderData.refusedOrders.length,
            itemBuilder: (ctx, i) => orderData.refusedOrders.elementAt(i));
  }
}
