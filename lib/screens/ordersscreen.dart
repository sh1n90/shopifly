import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopifly/providers/orders.dart';
import 'package:shopifly/screens/deliveredorder.dart';
import 'package:shopifly/screens/ordersinprogress.dart';
import 'package:shopifly/screens/orderstobeconfirmed.dart';
import 'package:shopifly/screens/refusedorder.dart';
import 'package:shopifly/screens/sentorders.dart';

class OrdersScreen extends StatelessWidget {
  static const routeName = '/orders-screen';

  Future<bool> cartsVendorSummary;
  Future<List<Map<String, dynamic>>> ordini;

  @override
  Widget build(BuildContext context) {
    print('build ordersScreen');
    final orderData = Provider.of<Orders>(context);

    return DefaultTabController(
        length: 5,
        child: Scaffold(
            appBar: AppBar(
              title: TabBar(
                tabs: [
                  Tab(icon: Icon(Icons.check), text: "Da confermare"),
                  Tab(
                      icon: Icon(Icons.directions_transit),
                      text: "In lavorazione"),
                  Tab(icon: Icon(Icons.directions_car), text: 'In consegna'),
                  Tab(icon: Icon(Icons.directions_transit), text: 'Completati'),
                  Tab(icon: Icon(Icons.dangerous), text: 'Rifiutati'),
                ],
              ),
            ),
            body: TabBarView(children: [
              OrdersToBeConfirmed(),
              OrdersInProgress(),
              SentOrders(),
              DeliveredOrders(),
              RefusedOrders(),
            ])));
  }
}
