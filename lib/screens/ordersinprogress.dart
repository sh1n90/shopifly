import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopifly/providers/orders.dart';

class OrdersInProgress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final orderData = Provider.of<Orders>(context, listen: false);
    return (orderData.inProgresOrders.isEmpty)
        ? FutureBuilder(
            future: orderData.getConfirmedOrders(),
            builder: (ctx, dataSnapshot) {
              if (dataSnapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                if (dataSnapshot.error != null) {
                  // ...
                  // Do error handling stuff
                  return Center(
                    child: Text('An error occurred!'),
                  );
                } else {
                  return ListView.builder(
                      itemCount: orderData.inProgresOrders.length,
                      itemBuilder: (ctx, i) =>
                          orderData.inProgresOrders.elementAt(i));
                  //    Text('ciao');
                }
              }
            })
        : ListView.builder(
            itemCount: orderData.inProgresOrders.length,
            itemBuilder: (ctx, i) => orderData.inProgresOrders.elementAt(i));
  }
}
