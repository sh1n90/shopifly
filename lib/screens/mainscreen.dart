import 'package:flutter/material.dart';

import 'package:shopifly/screens/homescreen.dart';
import 'package:shopifly/screens/ordersscreen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  static const routeName = '/home-screen';
  int _selectedIndex = 0;
  bool _is_home = true;
  bool _is_orders = false;
  bool _is_warehouse = false;

  void _onItemTapped(int index) {
    setState(() {
      print(index);
      _selectedIndex = index;
      switch (index) {
        case 0:
          _is_home = true;
          _is_orders = false;
          _is_warehouse = false;
          break;
        case 1:
          _is_home = false;
          _is_orders = true;
          _is_warehouse = false;
          break;
        case 2:
          _is_home = false;
          _is_orders = false;
          _is_warehouse = true;
          break;
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue.shade400,
        body: _is_home == true ? HomeScreen() : OrdersScreen(),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: const Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.business),
              label: 'Ordini',
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.fact_check_outlined),
              label: 'Inventario',
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.dehaze),
              label: 'Options',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          onTap: _onItemTapped,
        ));
  }
}
