import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:shopifly/providers/auth.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Container(
        width: deviceSize.width,
        height: deviceSize.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(
                    height: deviceSize.height / 5,
                    width: deviceSize.width / 8,
                  ),
                  Container(
                    height: deviceSize.height / 5,
                    width: deviceSize.width / 6,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('lib/assets/images/pet-shop.png'),
                        fit: BoxFit.fill,
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(
                    height: deviceSize.height / 5,
                    width: deviceSize.width / 8,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        children: [
                          Text(
                            '0\$',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: 'Song',
                                fontWeight: FontWeight.bold,
                                fontSize: deviceSize.height / 30),
                          ),
                          SizedBox(
                            width: deviceSize.width / 3,
                          ),
                          Text(
                            '1000\$',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: 'Song',
                                fontWeight: FontWeight.bold,
                                fontSize: deviceSize.height / 30),
                          ),
                        ],
                      ),
                      LinearPercentIndicator(
                        width: deviceSize.width / 2,
                        lineHeight: 8.0,
                        percent: 0.9,
                        progressColor: Colors.blue,
                      ),
                      SizedBox(
                        height: deviceSize.height / 15,
                        width: deviceSize.width / 8,
                      ),
                    ],
                  )
                ]),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              getFlatButton("Ordini da accettare", deviceSize, null, context),
              getFlatButton("ordini in consegna", deviceSize, null, context),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              getFlatButton("scatola uno", deviceSize, null, context),
              getFlatButton("scatola due", deviceSize,
                  Provider.of<Auth>(context, listen: false).logout, context),
            ]),
          ],
        ));
  }
}

FlatButton getFlatButton(
    String testo, Size _deviceSize, Function _onPress, BuildContext context) {
  return FlatButton(
    child: Container(
      height: _deviceSize.height / 5,
      width: _deviceSize.width / 3,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        //border: Border.all(color: Colors.black)
      ),
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            testo,
            style: TextStyle(
              fontFamily: 'Song',
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: _deviceSize.height / 25,
          ),
          Text(
            '8',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontFamily: 'Song',
                fontWeight: FontWeight.bold,
                fontSize: _deviceSize.height / 20),
          ),
        ],
      ),
    ),
    onPressed: _onPress,
  );
}
