import 'dart:convert';
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shopifly/api/api.dart';
import 'package:shopifly/models/http_exception.dart';
import 'package:shopifly/resources/user.dart';

class Auth with ChangeNotifier {
  bool get isAuth {
    return User().firebaseToken != null;
  }

  String get token {
    if (User().expiryDate != null &&
        User().expiryDate.isAfter(DateTime.now()) &&
        User().firebaseToken != null) {
      return User().firebaseToken;
    }
    return null;
  }

  String get userId {
    return User().userId;
  }

  Future<void> saveIntoPrefs() async {
    final prefs = await SharedPreferences.getInstance();

    print(User().expiryDate);

    final userData = json.encode(
      {
        'token': User().firebaseToken,
        'userId': User().userId,
        'expiryDate': User().expiryDate.toIso8601String(),
      },
    );

    print(userData.contains('token'));
    assert(await prefs.setString('userData', userData));
  }

  Future<void> signInWithGoogle() async {
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    final GoogleSignIn _googleSignIn = GoogleSignIn();
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      idToken: googleAuth.idToken,
      accessToken: googleAuth.accessToken,
    );
    await _firebaseAuth.signInWithCredential(credential);
    FirebaseUser user = await _firebaseAuth.currentUser();
    bool userSignedIn = await _googleSignIn.isSignedIn();

    assert(userSignedIn);
    final idToken = await user.getIdToken(refresh: true);

    User().firebaseToken = idToken.token;
    User().expiryDate = idToken.expirationTime;
    User().userId = user.uid;

    Response response = await Api.login();
    User().jwt = response.data['token'];

    notifyListeners();
    _autoLogout();
    saveIntoPrefs();
    return true;
  }

  Future<void> _authenticate(
      String email, String password, String urlSegment) async {
    final url =
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/$urlSegment?key=AIzaSyDlXILnoma1dfTBqnhTxNen2cSuSpTFEvM';
    try {
      final response = await http.post(
        url,
        body: json.encode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      print('i m here');
      User().firebaseToken = responseData['idToken'];
      print('tokeeeeeeeeeeeeeeeeeeeeeeeeeeeen ${User().firebaseToken}');
      User().userId = responseData['localId'];
      User().expiryDate = DateTime.now().add(
        Duration(
          seconds: int.parse(
            responseData['expiresIn'],
          ),
        ),
      );
      print(User().expiryDate);
      _autoLogout();
      saveIntoPrefs();
    } catch (error) {
      throw error;
    }
  }

  Future<void> signup(String email, String password) async {
    return _authenticate(email, password, 'signupNewUser');
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'verifyPassword');
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();

    print(prefs);

    print(!prefs.containsKey('userData'));
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    User().firebaseToken = extractedUserData['token'];
    User().userId = extractedUserData['userId'];
    User().expiryDate = expiryDate;
    notifyListeners();
    _autoLogout();
    return true;
  }

  Future<void> logout() async {
    User().firebaseToken = null;
    User().userId = null;
    User().expiryDate = null;
    if (User().authTimer != null) {
      User().authTimer.cancel();
      User().authTimer = null;
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    // prefs.remove('userData');
    prefs.clear();
  }

  void _autoLogout() {
    if (User().authTimer != null) {
      User().authTimer.cancel();
    }
    final timeToExpiry = User().expiryDate.difference(DateTime.now()).inSeconds;
    User().authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
