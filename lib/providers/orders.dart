import 'dart:core';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:shopifly/api/api.dart';
import 'package:shopifly/models/order.dart';

class Orders with ChangeNotifier {
  List<Order> _toBeconfirmedOrders = List<Order>();
  List<Order> _refusedOrders = List<Order>();
  List<Order> _confirmedOrders = List<Order>();
  List<Order> _sentOrders = List<Order>();
  List<Order> _delivered = List<Order>();

  Future<void> getToBeConfirmedOrders() async {
    Response res = await Api.getCustOrders(Api.to_be_confirmed);
    List<dynamic> map = res.data;
    map.forEach((element) {
      _toBeconfirmedOrders.add(new Order(
          idOrder: element['ID_ORDER'],
          totalAmount: element['TOTAL_AMOUNT'],
          idStatusDelivery: element['ID_STATUS_DELIVERY'],
          uniqueName: element['UNIQUE_NAME']));
    });
  }

  Future<void> getRefusedOrders() async {
    Response res = await Api.getCustOrders(Api.refused);
    List<dynamic> map = res.data;
    map.forEach((element) {
      _refusedOrders.add(new Order(
          idOrder: element['ID_ORDER'],
          totalAmount: element['TOTAL_AMOUNT'],
          idStatusDelivery: element['ID_STATUS_DELIVERY'],
          uniqueName: element['UNIQUE_NAME']));
    });
  }

  Future<void> getConfirmedOrders() async {
    Response res = await Api.getCustOrders(Api.confirmed);
    List<dynamic> map = res.data;
    map.forEach((element) {
      _confirmedOrders.add(new Order(
          idOrder: element['ID_ORDER'],
          totalAmount: element['TOTAL_AMOUNT'],
          idStatusDelivery: element['ID_STATUS_DELIVERY'],
          uniqueName: element['UNIQUE_NAME']));
    });
  }

  Future<void> getSentOrders() async {
    Response res = await Api.getCustOrders(Api.sent_orders);
    List<dynamic> map = res.data;
    map.forEach((element) {
      _sentOrders.add(new Order(
          idOrder: element['ID_ORDER'],
          totalAmount: element['TOTAL_AMOUNT'],
          idStatusDelivery: element['ID_STATUS_DELIVERY'],
          uniqueName: element['UNIQUE_NAME']));
    });
  }

  Future<void> getDelivered() async {
    Response res = await Api.getCustOrders(Api.delivered);
    List<dynamic> map = res.data;
    map.forEach((element) {
      _delivered.add(new Order(
          idOrder: element['ID_ORDER'],
          totalAmount: element['TOTAL_AMOUNT'],
          idStatusDelivery: element['ID_STATUS_DELIVERY'],
          uniqueName: element['UNIQUE_NAME']));
    });
  }

  List<Order> get toBeconfirmedOrders {
    return _toBeconfirmedOrders;
  }

  List<Order> get refusedOrders {
    return _refusedOrders;
  }

  List<Order> get inProgresOrders {
    return _confirmedOrders;
  }

  List<Order> get sentOrders {
    return _sentOrders;
  }

  List<Order> get delivered {
    return _delivered;
  }

  void setRefusedOrders(idOrder) async {
    await Api.setOrderStatus(Api.refused, idOrder);
    _toBeconfirmedOrders.removeWhere((element) => element.idOrder == idOrder);
  }

  void setConfirmOrders(idOrder) async {
    await Api.setOrderStatus(Api.confirmed, idOrder);
    _toBeconfirmedOrders.removeWhere((element) => element.idOrder == idOrder);
  }

  void setSentOrders(idOrder) async {
    await Api.setOrderStatus(Api.sent_orders, idOrder);
    _confirmedOrders.removeWhere((element) => element.idOrder == idOrder);
  }

  void setDelivered(idOrder) async {
    await Api.setOrderStatus(Api.delivered, idOrder);
    _confirmedOrders.removeWhere((element) => element.idOrder == idOrder);
  }
}
